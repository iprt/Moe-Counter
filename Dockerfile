FROM node:16

WORKDIR /app

COPY . .

RUN yarn config set registry https://registry.npmmirror.com

RUN yarn install

EXPOSE 3000

CMD ["yarn", "start"]
